// Hello! I hope this can help you in your exam! Goodluck! -Ms. Riza <3


// Question: What would be the console output of the function?

function checkGift(day) {
  let gifts = [
    'partridge in a pear tree',
    'turtle doves',
    'french hens',
    'golden rings'
  ];

  if (day > 0 && day < 4) {
    return `I was given ${day} ${gifts[day-1]}`;
  } else {
    return `No gifts were given`;
  }
}

checkGift(3);



// Question: What would be the problem in the code snippet?

let students = ['John', 'Paul', 'George', 'Ringo'];

console.log('Here are the graduating students:');

for (let count = 0; count <= students.length; i++) {
  console.log(students[count]);
}



// Question: What would be the output?

/*for (let row = 1; row < 3; row++) {
  for (let col = 1; col <= row; col++) {
    console.log(`Current row: ${row}, Current col: ${col}`);
  }
}

*/

// Question: What would be the problem in the code snippet?

/*function checkLeapYear(year) {
  if (year % 4 = 0) {
    if (year % 100 = 0) {
      if (year % 400 = 0) {
        console.log('Leap year');
      } else {
        console.log('Not a leap year');
      }
    } else {
      console.log('Leap year');
    }
  } else {
    console.log('Not a leap year');
  }
}

checkLeapYear(1999);*/



// Question: Given the array below, how can the last student's English grade be displayed?

/*let records = [
  {
    id: 1,
    name: 'Brandon',
    subjects: [
      { name: 'English', grade: 98 },
      { name: 'Math', grade: 66 },
      { name: 'Science', grade: 87 }
    ]
  },
  {
    id: 2,
    name: 'Jobert',
    subjects: [
      { name: 'English', grade: 87 },
      { name: 'Math', grade: 99 },
      { name: 'Science', grade: 74 }
    ]
  },
  {
    id: 3,
    name: 'Junson',
    subjects: [
      { name: 'English', grade: 60 },
      { name: 'Math', grade: 99 },
      { name: 'Science', grade: 87 }
    ]
  }
];*/